# HeroClock2

## Description

HeroClock2 is the new version of the web application [HeroClockr](https://gitlab.com/heroclock/heroclock2)

- It tracks personal goals and shows their ongoing duration
- Built as a showcase project with full CRUD
- Frontend built with [React](https://react.dev/) and [Material UI](https://mui.com/)
- Node.js api backend with MySQL

## Installation

HeroClockr can be run locally in the browser and stores streak names and dates on a locally running MySQL server database.

## Usage

- New streaks can be created when tapping the 'Attempt Streak' button
- Streaks can be edited, deleted with buttons

### Privacy

The stack itself is just meant to showcase and does not feature constantly evolving security measures

## Support

Feel free to contact me here on [GitLab](https://gitlab.com/cyaton).

## Feedback

Any feedback, suggestions or proper collaboration attempts are very welcome! Especially for other projects!

## Roadmap

Next possible step: Add in authentican and deploy on server

## Authors and acknowledgment

Contributors: Philipp Montazem

### Acknowledgments

#### Manik

Thanks to [Manik](https://cloudaffle.com/) for providing the [TypeScript Complete Course](https://www.udemy.com/course/typescript-course/) which heavily inspired this project.

#### Mozilla

As always, thanks to Mozilla for providing the [mdn resources for developers](https://developer.mozilla.org/en-US/).

## LICENSE

Copyright (C) 2023 Philipp Montazem

This Software is published under the GNU Affero General Public License v3.0. Please see [LICENSE.md](LICENSE.md) for this softwares specific license conditions.

## Project status

Project finished. Readme last updated on September 2023

