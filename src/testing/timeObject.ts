import {Duration} from "date-fns";
export const timeObject: Duration = {
    years: 2,
    months: 9,
    weeks: 1,
    days: 3,
}