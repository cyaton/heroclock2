import React from "react";
import {IDisabled} from "./iDisabled";

export interface ITextField extends IDisabled {
   onChange?: (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,) => void;
}