import * as React from 'react';
import { FC, ReactElement, useState, useEffect, useContext } from 'react';
import { useMutation } from '@tanstack/react-query';
import { Alert, AlertTitle, Button, LinearProgress } from '@mui/material';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import PropTypes from 'prop-types';
import { sendApiRequest } from '../../helpers/sendApiRequest';
import { format } from 'date-fns';

import { ITextField } from './interfaces/iTextField';
import { ICreateStreak } from '../streakList/interfaces/iCreateStreak';
import { StreakStatusChangedContext } from '../../context';

export const StartStreak: FC<ITextField> = (): ReactElement => {

  // States
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  //const [userId, setUserId] = useState<string | undefined>(
  //  undefined,
  //);
  const [title, setTitle] = useState<string | undefined>(
    undefined,
  );
  const [description, setDescription] = useState<string | undefined>(
    undefined,
  );

  // Create streak mutation
  const createStreakMutation = useMutation((data: ICreateStreak) =>
    sendApiRequest(
      'http://localhost:2456/streaks',
      'POST',
      data,
    ),
  );

  const [showSuccess, setShowSuccess] = useState<boolean>(false);

  const streaksUpdatedContext = useContext(
    StreakStatusChangedContext,
    );

  // Create streak handler function
  function createStreakHandler() {
    if (!title || !description) {
      return;
    }
    const streak: ICreateStreak = {
      user: 'a41de06b-7379-4db9-8a59-3fdf0fbea939',
      title,
      description,
      dateStarted: format(new Date(), 'yyyy-MM-dd'),
    };
    createStreakMutation.mutate(streak);
    handleClose();
  }

  // Manage side effects in app
  useEffect(() => {
    if(createStreakMutation.isSuccess) {
      setShowSuccess(true);
      streaksUpdatedContext.toggle();
    }
    const successTimeout = setTimeout(() => {
      setShowSuccess(false);
    }, 5000);

    return () => {
      clearTimeout(successTimeout);
    }
  }, [
    createStreakMutation.isSuccess
  ])
  return (
    <>
      {showSuccess && (
      <Alert
        severity='success'
        sx={{ width: '100%', marginBottom: '2rem', textAlign: 'left' }}
      >
        <AlertTitle>Success</AlertTitle>
        The task has been created successfully
      </Alert>
      )}
      <Button
        variant='contained'
        onClick={handleClickOpen}
      >
        Start a new Streak
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>New Streak</DialogTitle>
        <DialogContent>
          <DialogContentText>
            I am ready to start a new challenge. Nothing will stop me from reaching my goal.
          </DialogContentText>
          <TextField
            autoFocus
            margin='dense'
            id='title'
            label='Challenge Name'
            placeholder='Challenge Name'
            name='title'
            disabled={createStreakMutation.isLoading}
            value={title}
            type='text'
            fullWidth
            variant='standard'
            onChange={(e) => setTitle(e.target.value)}
          />
          <TextField
            autoFocus
            margin='dense'
            id='description'
            label='Challenge Description'
            placeholder='A challenge description'
            name='description'
            value={description}
            disabled={createStreakMutation.isLoading}
            type='text'
            fullWidth
            variant='standard'
            onChange={(e) => setDescription(e.target.value)}
          />
          {createStreakMutation.isLoading && <LinearProgress sx={{ mt: 6 }} />}
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleClose}
          >
            Cancel
          </Button>
          <Button
            variant='contained'
            disabled={
              !title || !description || createStreakMutation.isLoading
            }
            onClick={createStreakHandler}
          >
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

StartStreak.propTypes = {
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
};
export default StartStreak as FC;