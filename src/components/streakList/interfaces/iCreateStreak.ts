export interface ICreateStreak {
  user: string;
  title: string;
  description: string;
  dateStarted: string;
}