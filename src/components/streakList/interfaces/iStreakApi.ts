export interface IStreakApi {
  user: string;
  id: string;
  title: string;
  description: string;
  dateStarted: string;
}