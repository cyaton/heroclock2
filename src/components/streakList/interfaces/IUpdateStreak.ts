export interface IUpdateStreak {
  id: string;
  user: string;
  dateStarted?: string;
  title?: string;
  description?: string;
}