import React, { FC, ReactElement, useContext, useEffect } from 'react';
import { Alert, LinearProgress } from '@mui/material';
import Grid from '@mui/material/Unstable_Grid2';
import { Streak } from '../streak/streak';
import { useMutation, useQuery } from '@tanstack/react-query';
import { sendApiRequest } from '../../helpers/sendApiRequest';
import { IStreakApi } from './interfaces/iStreakApi';
import { IUpdateStreak } from './interfaces/IUpdateStreak';
import { IDeleteStreak } from './interfaces/IDeleteStreak'
import { StreakStatusChangedContext } from '../../context';
import { Duration, format, intervalToDuration } from 'date-fns';

export const StreakList: FC = (): ReactElement => {

  const calcStreakDuration = (dateStarted:string): Duration => {
    // Get the duration between date streak started and now
    return intervalToDuration({
      start: new Date(dateStarted),
      end: new Date()
    })
//Date object eg. => { years: 39, months: 2, days: 20, hours: 7, minutes: 5, seconds: 0 }
  }
  const streaksUpdatedContext = useContext(
    StreakStatusChangedContext
  );

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { error, isLoading, data, refetch } = useQuery(
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    ['streaks'],
    async () => {
      return await sendApiRequest<IStreakApi[]>(
        'http://localhost:2456/streaks',
        'GET',
      );
    },
  );

  // Update streak mutation
  const updateStreakMutation = useMutation(
    (data: IUpdateStreak) => sendApiRequest(
  'http://localhost:2456/streaks',
      'PUT',
      data,
    ),
  );

  const deleteStreakMutation = useMutation(
    (data: IDeleteStreak) => sendApiRequest(
      'http://localhost:2456/streaks',
      'DELETE',
      data,
    ),
  );

  useEffect(
    () => {
      refetch();
    },
    [streaksUpdatedContext.updated]
  );

  useEffect(() => {
    if(updateStreakMutation.isSuccess) {
      streaksUpdatedContext.toggle();
    }
  }, [updateStreakMutation.isSuccess]);

  useEffect(() => {
    if(deleteStreakMutation.isSuccess) {
      streaksUpdatedContext.toggle();
    }
  }, [deleteStreakMutation.isSuccess]);

  function resetStreakHandler(
    e:
      | React.MouseEvent<HTMLButtonElement>
      | React.MouseEvent<HTMLAnchorElement>,
    id: string,
    user: string,
  ) {
    updateStreakMutation.mutate({
      id,
      user,
      dateStarted: format(new Date(), "yyyy-MM-dd"),
    })
  }

  function deleteStreakHandler(
    e:
      | React.MouseEvent<HTMLButtonElement>
    | React.MouseEvent<HTMLAnchorElement>,
    id: string,
  ) {
    deleteStreakMutation.mutate({
      id,
    })
  }

  return (
    <Grid
      display='flex'
      flexDirection='column'
    >
      <>
        {error && (
          <Alert severity='error'>
            There was an error fetching your streaks
          </Alert>
        )}
        {!error && Array.isArray(data) && data.length === 0 && (
          <Alert severity='warning'>
            No streaks as of now. Start by creating a new glorious streak challenge.
          </Alert>
        )}

        {isLoading ? (
          <LinearProgress />
        ) : (
          Array.isArray(data) &&
          data.length > 0 &&
          data.map((each, index) => {
            return <Streak
              user='a41de06b-7379-4db9-8a59-3fdf0fbea939'
              key={'streak-' + index + each.title}
              id={each.id}
              title={each.title}
              duration={calcStreakDuration(each.dateStarted)}
              description={each.description}
              dateStarted={each.dateStarted}
              onClickReset={resetStreakHandler}
              onClickDelete={deleteStreakHandler}
            />;
          })
        )}
      </>
    </Grid>
  );
};

export default StreakList as FC;