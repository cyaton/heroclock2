import React, { FC, ReactElement } from "react";
import {Box, Chip, Typography} from "@mui/material";
import {IStreakHeader} from "./interfaces/IStreakHeader";
import {formatDuration} from "date-fns";
import PropTypes from "prop-types";

// TODO remove in Production
import {timeObject} from "../../testing/timeObject";

export const StreakHeader: FC<IStreakHeader> = (props): ReactElement => {

    //Destructure props
    const {
        title = 'Streak',
        duration = timeObject,
    } = props;



    return(
        <Box
            display="flex"
            width="100%"
            justifyContent="space-between"
        >
            <Box>
                <Typography variant="h2">{title}</Typography>
            </Box>
            <Box>
                <Chip
                    variant="outlined"
                    label={`${formatDuration(duration, { format: ['years', 'months', 'days', 'hours']})}`}
                />
            </Box>
        </Box>
    )
};

StreakHeader.propTypes = {
    title: PropTypes.string,
    duration: PropTypes.object,
}