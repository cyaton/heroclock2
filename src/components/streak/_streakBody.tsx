import React, {FC, ReactElement} from "react";
import {Box, Typography} from "@mui/material";
import {format} from "date-fns";
import {IStreakBody} from "./interfaces/IStreakBody";
import PropTypes from "prop-types";

export const StreakBody: FC<IStreakBody> = (props): ReactElement => {
    // Destructure props
    const {
        description = 'This is my new awesome streak',
        dateStarted = '2023-09-01',
    } = props;

    const dateObject = new Date(dateStarted);
    return (
        <>
            <Box>
                <Typography>{description}</Typography>
            </Box>
            <Box>
                <Typography
                    variant="caption"
                    color="grey"
                >Started {format(dateObject, 'PPP')}</Typography>
            </Box>
        </>
    )
};

StreakBody.propTypes = {
    description: PropTypes.string,
    dateStarted: PropTypes.string,
}

