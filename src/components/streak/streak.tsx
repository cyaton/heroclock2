import React, { FC, ReactElement } from 'react';
import { Box } from '@mui/material';
import { StreakHeader } from './_streakHeader';
import { StreakBody } from './_streakBody';
import { timeObject } from '../../testing/timeObject';
import { StreakFooter } from './_streakFooter';
import { IStreak } from './interfaces/IStreak';
import PropTypes from 'prop-types';

export const Streak: FC<IStreak> = (props): ReactElement => {

  // Destructure props
  const {
    title = 'Test Title',
    duration = timeObject,
    description = 'Mark as done.',
    dateStarted = "2023-09-01",
    onClickEdit = (e) => console.log(e),
    onClickReset = (e) => console.log(e),
    onClickDelete = (e) => console.log(e),
    id,
    user,
  } = props;
  return (
    <Box
      display='flex'
      width='100%'
      justifyContent='flex-start'
      flexDirection='column'
      mb={2}
      p={1.5}
      sx={{
        width: '100%',
        borderRadius: '8px',
        border: '1px solid grey',
      }}
    >
      <StreakHeader title={title} duration={duration} />
      <StreakBody description={description} dateStarted={dateStarted} />
      <StreakFooter
        onClickEdit={onClickEdit}
        onClickReset={onClickReset}
        onClickDelete={onClickDelete}
        id={id}
        user={user}
      />
    </Box>
  );
};

Streak.propTypes = {
  title: PropTypes.string,
  duration: PropTypes.object,
  description: PropTypes.string,
  dateStarted: PropTypes.string,
  onClickEdit: PropTypes.func,
  onClickReset: PropTypes.func,
  onClickDelete: PropTypes.func,
};