import React from "react";
export interface IStreakFooter {
   id: string;
   user: string;
   dateStarted?: string;
   onClickEdit?: (
       e:
         | React.MouseEvent<HTMLButtonElement>
         | React.MouseEvent<HTMLAnchorElement>
   ) => void;
   onClickReset?: (
       e:
         | React.MouseEvent<HTMLButtonElement>
         | React.MouseEvent<HTMLAnchorElement>,
       id: string,
       user: string,
   ) => void;
   onClickDelete?: (
       e:
         | React.MouseEvent<HTMLButtonElement>
         | React.MouseEvent<HTMLAnchorElement>,
       id: string,
   ) => void;
}