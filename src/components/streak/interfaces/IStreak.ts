import {IStreakHeader} from "./IStreakHeader";
import {IStreakBody} from "./IStreakBody";
import {IStreakFooter} from "./IStreakFooter";

export interface IStreak extends
    IStreakHeader,
    IStreakBody,
    IStreakFooter {
}