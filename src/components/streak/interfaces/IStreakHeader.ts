import {Duration} from "date-fns";

export interface IStreakHeader {
    title?: string;
    duration?: Duration;
}