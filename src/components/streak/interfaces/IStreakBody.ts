export interface IStreakBody {
    description?: string;
    dateStarted?: string;
}