import { Box, Button, ButtonGroup } from '@mui/material';
import React, { FC, ReactElement } from 'react';
import { IStreakFooter } from './interfaces/IStreakFooter';
import PropTypes from 'prop-types';

export const StreakFooter: FC<IStreakFooter> = (props): ReactElement => {

  // Destructure props
  const {
    id,
    user,
    onClickEdit = (e) => console.log(e),
    onClickReset = (e) => console.log(e),
    onClickDelete = (e) => console.log(e),
  } = props;

  return (
    <Box
      display='flex'
      justifyContent='space-evenly'
      alignItems='center'
      mt={2}
    >
      <ButtonGroup
        variant='contained'
        aria-label='outlined primary button group'
      >
        <Button onClick={(e) => onClickEdit(e)}>Edit</Button>
        <Button onClick={(e) => onClickReset(e, id, user)}>Reset</Button>
        <Button onClick={(e) => onClickDelete(e, id)}>Delete</Button>
      </ButtonGroup>
    </Box>
  );
};

StreakFooter.propTypes = {
  onClickEdit: PropTypes.func,
  onClickReset: PropTypes.func,
  onClickDelete: PropTypes.func,
  id: PropTypes.string.isRequired,
  user: PropTypes.string.isRequired,
};