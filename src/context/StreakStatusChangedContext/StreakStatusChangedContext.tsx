import React, { createContext, FC, ReactElement, PropsWithChildren, useState } from 'react';

export const StreakStatusChangedContext = createContext({
  updated: false,
  toggle: () => {},
});

export const StreakStatusChangedContextProvider: FC<PropsWithChildren> = (props): ReactElement => {
  const [updated, setUpdated] = useState(false);

  function toggleHandler() {
    updated ? setUpdated(false) : setUpdated(true);
  }

  return <StreakStatusChangedContext.Provider value={{
    updated: updated,
    toggle: toggleHandler,
  }}>
    {props.children}
  </StreakStatusChangedContext.Provider>
};