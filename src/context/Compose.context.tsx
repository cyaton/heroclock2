import React, {FC, ReactNode } from 'react';

interface IComposeContext {
  // Components have optional property of any number of children
  components?: FC<{children?:ReactNode}>[];
  children?: ReactNode | undefined;
}
export default function ComposeContext(props: IComposeContext){
  // Destructure props
  const { components = [], children } = props;

  return <>
    {
      components.reduceRight((acc, Comp) => {
        return <Comp>{acc}</Comp>
      }, children)
    }
  </>
}