import React, { FC, ReactElement } from 'react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { Box, ThemeProvider } from '@mui/material';
import { CssBaseline } from '@mui/material';
import { setMainTheme } from './theme/mainTheme';
import { Home } from './pages/home/home';
import Grid from '@mui/material/Unstable_Grid2';
import ComposeContext from './context/Compose.context';
import { rootContext } from './context/root.context';

// Create client
const queryClient = new QueryClient();

const App: FC = (): ReactElement => {


  return (
    <QueryClientProvider client={queryClient}>
      <ComposeContext components={rootContext}>
        <ThemeProvider theme={setMainTheme()}>
          <CssBaseline />
          <Grid container alignItems='center' xs={12} height='100vh'>
            <Grid xs={12}>
              <Box
                paddingX={0.5}
                maxWidth='800px'
                marginLeft='auto'
                marginRight='auto'
              >
                <Home />
              </Box>
            </Grid>
          </Grid>
        </ThemeProvider>
      </ComposeContext>
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  );
};

export default App;