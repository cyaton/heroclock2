import * as React from 'react';
import useMediaQuery from '@mui/material/useMediaQuery';
import {createTheme} from "@mui/material";
import '@fontsource/racing-sans-one';
// Supports weights 100-900
import '@fontsource-variable/rokkitt';

export function setMainTheme() {
    const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

    const theme = React.useMemo(
        () =>
            createTheme({
                palette: {
                    mode: prefersDarkMode ? 'dark' : 'light',
                    primary: {
                        light: '#FFC936',
                        main: '#FFBC04',
                        dark: '#B28302',
                    },
                    secondary: {
                        light: '#366bff',
                        main: '#0447ff',
                        dark: '#0231b2',
                    }
                },
                typography: {
                    fontFamily: [
                        'Rokkitt Variable',
                        'sans-serif',
                    ].join(','),
                }
            }),
        [prefersDarkMode],
    )

    theme.typography.h1 = {
        fontFamily: [
            'Racing Sans One',
            'sans-serif',
        ].join(','),
        fontSize: '1.5rem',
        '@media (min-width:600px)': {
            fontSize: '2rem',
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '2.5rem',
        },
    };

    theme.typography.h2 = {
        fontFamily: [
            'Rokkitt Variable',
            'sans-serif',
        ].join(','),
        fontSize: '1rem',
        '@media (min-width:600px)': {
            fontSize: '1rem',
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '1.2rem',
        },
    };
    return theme;
}

