import React, {FC, ReactElement, lazy} from "react";
import {Typography, Box} from "@mui/material";

const StreakList: FC = lazy(
    () => import("../../components/streakList/streakList")
        .then(({default: StreakList}) => ({default: StreakList})
        ));

const StartStreak: FC = lazy(
    () => import("../../components/startStreak/startStreak")
        .then(({default: StartChallenge}) => ({default: StartChallenge})
        ));

export const Home: FC = (): ReactElement => {

    return (
        <>
            <Typography
                align="center"
                variant="h1"
                mb={1}
            >
                Hero Clock 2
            </Typography>
            <Typography
                align="center"
                mb={4}
            >
                Accomplish anything
            </Typography>
            <Box
                textAlign='center'
                mb={4}
            >
                <StartStreak/>
            </Box>
            <StreakList/>
        </>
    )
};